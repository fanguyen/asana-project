import React, { Component } from "react";
import "./TodoHeader.css";
import ProjetsList from "../ProjetsList/ProjetsList";

export default class TodoHeader extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
    this.state = { displayButton: false };
  }

  handleAddProjet = event => {
    event.preventDefault();
    this.props.handleAddProjet(this.inputRef.current.value);
    this.inputRef.current.value = "";
  };

  displayButton = () => {
    return this.inputRef.current && this.inputRef.current.value.length > 0;
  };

  handleOnChange = () => {
    const displayButton = this.displayButton();
    this.setState({ displayButton });
  };

  hideButton = () => {
    this.setState({ displayButton: false, inputValue: "" });
  };

  render() {
    console.log("render");
    return (
      <form
        className="form-inline header-add-item"
        onSubmit={this.handleAddProjet}
      >
        <div className="header-item-input">
          <input
            className="form-control form-control-sm"
            type="text"
            onChange={this.handleOnChange}
            onBlur={this.hideButton}
            ref={this.inputRef}
          />
          {/* {this.state.displayButton ? (
            <button className="btn btn-primary btn-sm" type="submit">
              Add
            </button>
          ) : null} */}
        </div>
      </form>
    );
  }
}
