import React, { Component } from "react";
import "./TodoHeader.css";

export default class TodoAddHeader extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();

    this.state = { open: false, isValidTodo: false };
  }

  handleAddTodo = event => {
    event.preventDefault();
    if (this.isValidTodo()) {
      this.setState({ open: false });
      this.props.handleAddTodo(
        this.inputRef.current.value,
        this.props.columnName
      );
    }
  };

  isValidTodo = () => {
    return this.inputRef.current && this.inputRef.current.value.length > 0;
  };

  handleOnChange = () => {
    const isValidTodo = this.isValidTodo();
    this.setState({ isValidTodo });
  };

  displayInput = () => {
    this.setState({ open: true }, () => this.inputRef.current.focus());
  };
  hideInput = () => {
    this.setState({ open: false });
  };

  render() {
    return (
      <div className="header-item-valider">
        {this.state.open ? (
          <form className="form-inline" onSubmit={this.handleAddTodo}>
            <input
              className="form-control form-control-sm"
              type="text"
              defaultValue=""
              onChange={this.handleOnChange}
              ref={this.inputRef}
              onBlur={this.hideInput}
            />
            {/* {this.state.isValidTodo ? ( */}
            {/* <button className="btn btn-primary btn-sm" type="submit">
                Add
              </button> */}
            {/* ) : null} */}
          </form>
        ) : (
          <input
            className="header-item"
            type="button"
            value={"+"}
            onClick={this.displayInput}
          />
        )}
      </div>
    );
  }
}
