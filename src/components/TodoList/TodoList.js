import React, { Component } from "react";
import Todo from "../Todo/Todo";

import "./TodoList.css";

//Equivaut à
// const Todolist = ({ filter, changeStatus, todos, handleDelete }) => (
//   <div className="TodoList" id={`TodoList-${filter}`}>
//     {todos
//       .filter(todo => filter === "all" || todo.status === filter)
//       .map(todo => (
//         <Todo
//           key={todo.id}
//           todo={todo}
//           handleDelete={id => this.props.handleDelete(id)}
//         />
//       ))}
//   </div>
// );

export default class TodoList extends Component {
  render() {
    const { id, todos, deleteTodo, updateTextTodo } = this.props;
    return (
      <div className="TodoList" id={id}>
        {todos.map(todo => (
          <Todo
            key={todo.id}
            todo={todo}
            deleteTodo={deleteTodo}
            updateTextTodo={updateTextTodo}
          />
        ))}
      </div>
    );
  }
}
