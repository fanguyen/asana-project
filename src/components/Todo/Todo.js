import React, { Component } from "react";
import "./Todo.css";

import FaEdit from "react-icons/lib/io/edit";
import FaCross from "react-icons/lib/io/close-round";

export default class Todo extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
    this.state = {
      edit: false,
      delete: false
    };
  }

  toggleEdit = () => {
    console.log("Click");
    this.setState({ edit: !this.state.edit });
  };

  updateTodo = event => {
    event.preventDefault();
    this.props.updateTextTodo(this.props.todo.id, this.inputRef.current.value);
    this.toggleEdit();
  };

  displayInput = () => {
    this.setState({ edit: true }, () => this.inputRef.current.focus());
  };

  hideInput = () => {
    this.setState({ edit: false });
  };

  render() {
    const { todo } = this.props;
    return (
      <div className="Todo" id={`Todo-${todo.id}`}>
        {this.state.edit ? (
          <form className="form-inline" onSubmit={this.updateTodo}>
            <input
              className="form-control form-control-sm"
              type="text"
              defaultValue={todo.text}
              ref={this.inputRef}
              onBlur={this.hideInput}
            />
            {/* <button type="submit" className="btn btn-primary btn-sm">
              Submit
            </button> */}
          </form>
        ) : (
          <div className="user-content-name">
            <span>{todo.text}</span>
          </div>
        )}
        <FaEdit className="icon-edit" onClick={this.displayInput} />
        <FaCross
          className="icon-delete"
          onClick={() => this.props.deleteTodo(todo.id)}
        />
      </div>
    );
  }
}
