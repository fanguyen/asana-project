import React, { Component } from "react";
import "./ProjetsList.css";

import FaEdit from "react-icons/lib/io/edit";
import FaCross from "react-icons/lib/io/close-round";

export default class ProjetsList extends Component {
  constructor(props) {
    super(props);
    this.state = { edit: false, delete: false };
    this.inputRef = React.createRef();
  }
  getClass = id => {
    return this.props.selectedProjet === id ? "btn-primary" : "btn-secondary";
  };

  toggleEdit = () => {
    console.log("Click");
    this.setState({ edit: !this.state.edit });
  };

  render() {
    return this.props.projets.map(projet => (
      <div className="ProjetList" key={projet.id}>
        <div className="projet-card">
          {this.state.edit ? (
            <form className="form-inline" onSubmit={this.updateProjet}>
              <input
                ref={this.inputRef}
                className="form-control form-control-sm"
                type="text"
                defaultValue={projet.text}
              />
              {/* <button type="submit" className="btn btn-primary btn-sm">
                Submit
              </button> */}
              <FaCross
                type="button"
                value="del"
                className="icon-delete-projet"
                onClick={() => this.props.deleteProjet(projet.id)}
              />
            </form>
          ) : (
            <div className="user-content-name">
              <span>{projet.text}</span>
              <input
                className={`btn ${this.getClass(projet.id)}`}
                type="button"
                value={projet.projet}
                onClick={() => this.props.selectProjet(projet)}
              />
            </div>
          )}
        </div>

        <FaEdit className="icon-edit-projet" onClick={this.toggleEdit} />
      </div>
    ));
  }
}
