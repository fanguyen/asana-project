import React, { Component } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import Dragula from "react-dragula";
import TodoList from "./components/TodoList/TodoList";
import TodoHeader from "./components/TodoHeader/TodoHeader";
import ProjetsList from "./components/ProjetsList/ProjetsList";
import TodoAddHeader from "./components/TodoHeader/TodoAddHeader";

/* const initialTodos = [
  { id: 0, text: "Buy Bananas", status: "active" },
  { id: 1, text: "Fishing snappers", status: "active" },
  { id: 2, text: "visit mom", status: "active" }
]; */

/* const initialProjets = [
  {
    id: 0,
    projet: "Projet A",
    todos: [
      { id: 0, text: "Buy Bananas", status: "active" },
      { id: 1, text: "Fishing snappers", status: "active" },
      { id: 2, text: "Fishing snappers2", status: "inprogress" },
      { id: 3, text: "Fishing snappers3", status: "completed" },
      { id: 4, text: "Fishing snappers4", status: "completed" }
    ]
  },
  {
    id: 1,
    projet: "Projet B",
    todos: [{ id: 2, text: "visit mom", status: "active" }]
  }
]; */

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projets: [],
      selectedProjet: 0,
      loaded: false
    };
    this.columns = [
      { name: "To do", filter: "active" },
      { name: "In progress", filter: "inprogress" },
      { name: "Completed", filter: "completed" }
    ];
    this.inputRef = React.createRef();
    this.selectRef = React.createRef();
  }

  componentDidMount() {
    this.hydrateStateWithLocalStorage();
  }

  hydrateStateWithLocalStorage() {
    // for all items in state
    for (let key in this.state) {
      // if the key exists in localStorage
      if (localStorage.hasOwnProperty(key)) {
        // get the key's value from localStorage
        let value = localStorage.getItem(key);

        // parse the localStorage string and setState
        try {
          value = JSON.parse(value);
          this.setState({ [key]: value });
        } catch (e) {
          // handle empty string
          this.setState({ [key]: value });
        }
      }
    }
    this.setState({ loaded: true });
  }

  updateInput(key, value) {
    // update react state
    this.setState({ [key]: value });

    // update localStorage
    localStorage.setItem(key, value);
  }

  handleAddTodo = (value, status = "active") => {
    console.log("Add todo");
    let projets = this.state.projets;
    const id = Date.now();
    {
      let activeProjet = projets.filter(
        projet => projet.id === this.state.selectedProjet
      )[0];

      activeProjet.todos.push({
        id,
        text: value,
        status
      });

      projets = [...projets, ...activeProjet];
      this.setState({ projets, inputValue: "" });
    }
    localStorage.setItem("projets", JSON.stringify(projets));
  };

  handleAddProjet = value => {
    console.log("Add projet");
    let projets = this.state.projets;
    const id = Date.now();
    {
      projets.push({
        id,
        projet: value,
        todos: []
      });
      this.setState({ projets, inputValue: "", selectedProjet: id });
      localStorage.setItem("selectedProjet", JSON.stringify(id));
    }
    localStorage.setItem("projets", JSON.stringify(projets));
  };

  getSelectedProjet = () => {
    return this.state.projets.find(
      projet => projet.id === this.state.selectedProjet
    );
  };

  changeStatus = (todoId, newStatus) => {
    let projets = this.state.projets;

    let activeProjet = this.getSelectedProjet();

    activeProjet.todos = activeProjet.todos.map(todo => {
      if (todo.id === todoId) {
        return {
          ...todo,
          status: newStatus
        };
      }
      return todo;
    });

    projets = [...projets, ...activeProjet];
    this.setState({ projets });
    localStorage.setItem("projets", JSON.stringify(projets));
  };

  selectProjet = projet => {
    this.setState({ selectedProjet: projet.id });
    localStorage.setItem("selectedProjet", JSON.stringify(projet.id));
  };

  deleteTodo = id => {
    const projets = this.state.projets.map(projet => {
      if (projet.id === this.state.selectedProjet) {
        return {
          ...projet,
          todos: projet.todos.filter(todo => todo.id !== id)
        };
      }
      return projet;
    });

    this.setState({ projets });
    localStorage.setItem("projets", JSON.stringify(projets));
  };

  deleteProjet = id => {
    if (id === this.state.selectedProjet) {
      const selectedProjet = this.state.projets[0]
        ? this.state.projets[0].id
        : 0;
      this.setState({ selectedProjet });
    }
    const projets = this.state.projets.filter(projet => projet.id !== id);
    this.setState({ projets });
    localStorage.setItem("projets", JSON.stringify(projets));
  };

  updateTextTodo = (id, text) => {
    let projets = this.state.projets;

    let activeProjet = this.getSelectedProjet();

    activeProjet.todos = activeProjet.todos.map(todo => {
      if (todo.id === id) {
        return {
          ...todo,
          text
        };
      }
      return todo;
    });

    projets = [...projets, ...activeProjet];
    this.setState({ projets });
    localStorage.setItem("projets", JSON.stringify(projets));
  };

  dragulaDecorator = componentBackingInstance => {
    if (componentBackingInstance) {
      let options = {
        isContainer: function(el) {
          return false; // only elements in drake.containers will be taken into account
        },
        moves: function(el, source, handle, sibling) {
          return el.classList.contains("Todo"); // elements are always draggable by default
        },
        accepts: function(el, target, source, sibling) {
          return target.classList.contains("TodoList"); // elements can be dropped in any of the `containers` by default
        },
        invalid: function(el, handle) {
          return false; // don't prevent any drags from initiating by default
        },
        direction: "vertical", // Y axis is considered when determining where an element would be dropped
        copy: true, // elements are moved by default, not copied
        copySortSource: false, // elements in copy-source containers can be reordered
        revertOnSpill: false, // spilling will put the element back where it was dragged from, if this is true
        removeOnSpill: true, // spilling will `.remove` the element, if this is true
        mirrorContainer: document.body, // set the element that gets mirror elements appended
        ignoreInputTextSelection: true // allows users to select input text, see details below
      };

      let drake = Dragula(
        [
          componentBackingInstance,
          document.querySelector("#TodoList-active"),
          document.querySelector("#TodoList-inprogress"),
          document.querySelector("#TodoList-completed")
        ],
        options
      );

      //   drake.containers.push(dropZone);
      drake.on("drop", (el, target) => {
        el.remove();
        const id = el.id.split("Todo-")[1];
        const status = target.id.split("TodoList-")[1];
        this.changeStatus(parseInt(id, 10), status);
      });
    }
  };

  enableTodoSelect = () => this.state.projets.length > 0;

  getFilteredTodos = filter => {
    const selectedProjet = this.getSelectedProjet();
    return selectedProjet.todos.filter(
      todo => filter === "all" || todo.status === filter
    );
  };

  render() {
    const activeProjet = this.getSelectedProjet();
    return this.state.loaded ? (
      <div className="App">
        <header className="App-header" />
        <TodoHeader
          handleAddProjet={this.handleAddProjet}
          enableTodoSelect={this.enableTodoSelect()}
        />
        {activeProjet !== undefined ? (
          <div className=" container-status">
            <div className="project-container">
              <ProjetsList
                projets={this.state.projets}
                selectProjet={this.selectProjet}
                selectedProjet={this.state.selectedProjet}
                deleteProjet={this.deleteProjet}
              />
            </div>

            <div className="list-status" ref={this.dragulaDecorator}>
              {this.columns.map(column => (
                <div
                  className={`list-status-${column.filter}`}
                  key={column.filter}
                >
                  <div className="list-status-title">{column.name}</div>
                  <TodoAddHeader
                    handleAddTodo={this.handleAddTodo}
                    enableTodoSelect={this.enableTodoSelect()}
                    columnName={column.filter}
                  />
                  <br />
                  <br />
                  <hr />
                  <TodoList
                    todos={this.getFilteredTodos(column.filter)}
                    id={`TodoList-${column.filter}`}
                    deleteTodo={this.deleteTodo}
                    updateTextTodo={this.updateTextTodo}
                  />
                </div>
              ))}
            </div>
          </div>
        ) : null}
      </div>
    ) : null;
  }
}

export default App;
